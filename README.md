# Technical test

A new Flutter Omni technical test project.

## Getting Started

This technical test can be run in Web, Linux and Android Platform.\
You must download the [Flutter SDK](https://docs.flutter.dev/get-started/install).


### Android
For Android you must download [Android Studio](https://developer.android.com/studio).
#### debug
`flutter run`
#### profile
`flutter run --profile`
#### release
`flutter run --release`

### Web
#### debug
`flutter run -d chrome --web-renderer html`
#### profile
`flutter run -d chrome --web-renderer html --profile`
#### release
`flutter run -d chrome --web-renderer html --release`

### Linux
#### debug
`flutter run -d linux`
#### profile
`flutter run -d linux --profile`
#### release
`flutter run -d linux --release`
