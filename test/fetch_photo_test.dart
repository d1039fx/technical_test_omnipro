import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:technical_test/domain/models/photos_model/photos_model.dart';
import 'package:technical_test/infraestructure/driven_adapter/api/errors/photos_data_api_error.dart';
import 'package:technical_test/infraestructure/driven_adapter/api/photos_data_api/photos_data_api.dart';

import 'fetch_photo_test.mocks.dart';

@GenerateMocks([http.Client])
void main() {
  group('fetchAlbum', () {
    test('return on Photos if http call completes successfully', () async {
      final client = MockClient();
      when(client.get(Uri.parse(
              'https://jsonplaceholder.typicode.com/photos?_start=10&_limit=10')))
          .thenAnswer((_) async => http.Response(
              '[{"albumId": 1, "id": 1, "title": "mock", "url": "https://via.placeholder.com/600/92c952", "thumbnailUrl": "https://via.placeholder.com/150/92c952"}]',
              200));
      expect(
          await PhotosDataApi()
              .getPhotosPaginationData(pagination: 1, client: client),
          isA<List<PhotosModel>>());
    });

    test('throws an exception if the http call completes with an error', () {
      final client = MockClient();
      when(client.get(Uri.parse(
              'https://jsonplaceholder.typicode.com/photos?_start=0&_limit=10')))
          .thenAnswer((_) async => http.Response('Not Found', 404));

      expect(
          PhotosDataApi()
              .getPhotosPaginationData(pagination: 0, client: client),
          throwsA(PhotoDataApiError().toString()));
    });
  });
}
