import 'dart:convert';

import 'package:technical_test/domain/models/photos_model/photos_model.dart';
import 'package:technical_test/domain/models/photos_model/repository/photos_repository.dart';
import 'package:http/http.dart' as http;
import 'package:technical_test/infraestructure/driven_adapter/api/errors/photos_data_api_error.dart';

class PhotosDataApi extends PhotosRepository {
  @override
  Future<List<PhotosModel>> getPhotosPaginationData(
      {required int pagination, required http.Client client}) async {
    Uri url = Uri.parse(
        'https://jsonplaceholder.typicode.com/photos?_start=${pagination * 10}&_limit=10');
    final response = await client.get(url);
    if (response.statusCode == 200) {
      List dataList = jsonDecode(response.body);
      List<PhotosModel> photoList =
          dataList.map((photo) => PhotosModel.fromJson(photo)).toList();
      return photoList;
    } else {
      throw PhotoDataApiError().toString();
    }
  }
}
