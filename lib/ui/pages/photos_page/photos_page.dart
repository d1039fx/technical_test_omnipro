import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:technical_test/config/blocs/photo_data/photo_data_bloc.dart';
import 'package:technical_test/domain/models/photos_model/photos_model.dart';

class PhotosPage extends StatefulWidget {
  const PhotosPage({super.key});

  @override
  State<PhotosPage> createState() => _PhotosPageState();
}

class _PhotosPageState extends State<PhotosPage> {
  List<PhotosModel> photoList = [];
  ScrollController controller = ScrollController();

  @override
  void initState() {
    controller.addListener(() {
      if (controller.offset == controller.position.maxScrollExtent) {
        context.read<PhotoDataBloc>().add(PhotoPagination());
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PhotoDataBloc, PhotoDataState>(
      listener: (context, state) {
        if (state is PaginationPhotoList) {
          photoList.addAll(state.photoList);
        }
      },
      builder: (context, state) {
        if (state is LoadingInitPhotoList) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        if (state is ErrorPhotoList) {
          return Center(
            child: Text(state.errorPhotoList),
          );
        }
        return Stack(
          children: [
            ListView.builder(
                controller: controller,
                itemCount: photoList.length,
                itemBuilder: (context, index) {
                  PhotosModel photoModel = photoList[index];
                  return ListTile(
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(50),
                      child: FadeInImage.assetNetwork(placeholder: 'assets/images/loader.png', image: photoModel.thumbnailUrl),
                    ),
                    title: Text(photoModel.title),
                    subtitle: Text(photoModel.id.toString()),
                  );
                }),
            state is LoadingPaginationPhotoList
                ? const Align(
                    alignment: Alignment.bottomCenter,
                    child: LinearProgressIndicator(),
                  )
                : const SizedBox(),
          ],
        );
      },
    );
  }
}
