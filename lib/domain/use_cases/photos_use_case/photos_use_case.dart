import 'package:technical_test/domain/models/photos_model/photos_model.dart';
import 'package:technical_test/domain/models/photos_model/repository/photos_repository.dart';
import 'package:http/http.dart' as http;

class PhotosUseCase {
  final PhotosRepository photosRepository;

  PhotosUseCase({required this.photosRepository});

  Future<List<PhotosModel>> getPhotosPaginationData({required int pagination, required http.Client client}) =>
      photosRepository.getPhotosPaginationData(pagination: pagination, client: client);
}
