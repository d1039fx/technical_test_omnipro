import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'photos_model.freezed.dart';
part 'photos_model.g.dart';

@freezed
class PhotosModel with _$PhotosModel {
  const factory PhotosModel({
    required int albumId,
    required int id,
    required String title,
    required String url,
    required String thumbnailUrl
  }) = _PhotosModel;



  factory PhotosModel.fromJson(Map<String, dynamic> json) =>
      _$PhotosModelFromJson(json);
}