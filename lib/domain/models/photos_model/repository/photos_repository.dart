import 'package:technical_test/domain/models/photos_model/photos_model.dart';
import 'package:http/http.dart' as http;

abstract class PhotosRepository{
  Future<List<PhotosModel>> getPhotosPaginationData({required int pagination, required http.Client client});
}