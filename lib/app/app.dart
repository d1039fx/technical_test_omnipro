import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:technical_test/config/blocs/photo_data/photo_data_bloc.dart';
import 'package:technical_test/ui/pages/photos_page/photos_page.dart';

class TechnicalTest extends StatefulWidget {
  const TechnicalTest({super.key});

  @override
  State<TechnicalTest> createState() => _TechnicalTestState();
}

class _TechnicalTestState extends State<TechnicalTest> {
  @override
  void initState() {
    context.read<PhotoDataBloc>().add(InitPhotoPagination());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Fotos'),
      ),
      body: const PhotosPage(),
    );
  }
}
