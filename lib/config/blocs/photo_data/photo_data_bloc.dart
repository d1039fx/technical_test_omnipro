import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:technical_test/domain/models/photos_model/photos_model.dart';
import 'package:technical_test/domain/use_cases/photos_use_case/photos_use_case.dart';
import 'package:technical_test/infraestructure/driven_adapter/api/errors/photos_data_api_error.dart';
import 'package:technical_test/infraestructure/driven_adapter/api/photos_data_api/photos_data_api.dart';
import 'package:http/http.dart' as http;

part 'photo_data_event.dart';
part 'photo_data_state.dart';

class PhotoDataBloc extends Bloc<PhotoDataEvent, PhotoDataState> {
  PhotosUseCase photosUseCase = PhotosUseCase(photosRepository: PhotosDataApi());
  int pagination = 1;

  PhotoDataBloc() : super(PhotoDataInitial()) {
    on<InitPhotoPagination>((event, emit) async {
      emit(LoadingInitPhotoList());
      try {
        List<PhotosModel> photoList =
            await photosUseCase.getPhotosPaginationData(pagination: 0,client: http.Client());
        emit(PaginationPhotoList(photoList: photoList));
      } catch (exception) {
        emit(ErrorPhotoList(errorPhotoList: PhotoDataApiError().toString()));
      }
    });

    on<PhotoPagination>((event, emit) async {
      emit(LoadingPaginationPhotoList());
      List<PhotosModel> photoList = await photosUseCase
          .getPhotosPaginationData(pagination: pagination++, client: http.Client())
          .onError((error, stackTrace) {
        emit(ErrorPhotoList(errorPhotoList: error.toString()));
        return [];
      });
      emit(PaginationPhotoList(photoList: photoList));
    });
  }
}
