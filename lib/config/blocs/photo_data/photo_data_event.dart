part of 'photo_data_bloc.dart';


abstract class PhotoDataEvent {}

class InitPhotoPagination extends PhotoDataEvent{}
class PhotoPagination extends PhotoDataEvent{}