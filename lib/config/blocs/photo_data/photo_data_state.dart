part of 'photo_data_bloc.dart';

abstract class PhotoDataState {}

class PhotoDataInitial extends PhotoDataState {}

class LoadingInitPhotoList extends PhotoDataState{}

class InitPhotoList extends PhotoDataState{
  final List<PhotosModel> photoList;

  InitPhotoList({required this.photoList});
}

class ErrorPhotoList extends PhotoDataState{
  final String errorPhotoList;

  ErrorPhotoList({required this.errorPhotoList});
}

class LoadingPaginationPhotoList extends PhotoDataState{}

class PaginationPhotoList extends PhotoDataState{
  final List<PhotosModel> photoList;

  PaginationPhotoList({required this.photoList});

}